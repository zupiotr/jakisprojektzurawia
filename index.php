<?php get_header(); ?>
<div class="container">
	<div class="row r1">
	
		<div class="col-md-10 col-xs-12 col1">
				<div id="dupa">
				<h1>Main Areasaddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</h1>
				</div>


				<div class="posty">
			
					<?php if ( have_posts() ) : 
								while ( have_posts() ) : the_post(); ?>
			
					  <div class="media">
						<div class="obra">
							<a class="pull-left" href="<?php the_permalink(); ?>">
							  <?php the_post_thumbnail( $size = 'thumbnail'); ?>
							</a>
						</div>
						  <div class="media-body">
							  <a href="<?php the_permalink(); ?>"><h4 class="media-heading"><?php the_title(); ?></h4></a>
							  <h4>Opublikowano <?php the_time('j F Y | g:i') ?><br> Autor: <?php the_author(); ?></h4>
							  <?php the_excerpt(); ?>
							  <a href="<?php the_permalink(); ?>" class="btn btn-default">Czytaj więcej</a>
						  </div>
					  </div>
					  
					  <br>
					  
					  <?php endwhile; ?>
					  <!-- post navigation -->
					  <?php else: ?>
						<p><?php _e('Nie znaleziono żadnych postów'); ?></p>
					  <!-- no posts found -->
					 <?php endif; ?>
				</div>
				
				
				
		</div> <!--col-md-10-->
	
		<div class="col-md-2 col-xs-12 col2">
				<br>
				<?php get_sidebar(); ?>
		</div> <!--col-lg-3-->
	</div> <!--row-->
	<div class="row r2">
<!--AKTUALNOŚCI LEWA-->
      <div class="col-md-6">
	  <br>
	  <br>
        <div class="panel panel-default">
         <div class="panel-heading">
           <h3 class="panel-title">Aktualności lewa</h3>
         </div>
         <div class="panel-body">
      <!-- ostatnio dodany post -->
         <?php
         $args = array( 'numberposts' => 2, 'order'=> 'DESC', 'orderby' => 'date', 'category_name' => 'aktualnosci-lewa' );
         $postslist = get_posts( $args );
         foreach ($postslist as $post) :  setup_postdata($post); ?> 
           <div class="media">
             <a class="pull-left" href="<?php the_permalink(); ?>">
               <?php 
                 if ( has_post_thumbnail() ) {
                   the_post_thumbnail('thumbnail');
                 } 
               ?>
             </a>
             <div class="media-body">
               <a href="<?php the_permalink(); ?>"><strong><?php the_title(); ?> </strong></a>
               <small><?php the_time('j F, Y'); ?></small>
               <?php the_excerpt(); ?>
               <a href="<?php the_permalink(); ?>" class="btn btn-info">Czytaj więcej!</a>
             </div>
           </div>
           <hr>
           <?php endforeach; ?>
          </div>
        </div>
      </div> <!--col-md-6-->
<!--AKTUALNOSCI PRAWA-->
      <div class="col-md-6">
	  <br>
	  <br>
        <div class="panel panel-default">
         <div class="panel-heading">
           <h3 class="panel-title">Aktualności prawa</h3>
         </div>
         <div class="panel-body">
      <!-- ostatnio dodany post -->
         <?php
         $args = array( 'numberposts' => 2, 'order'=> 'DESC', 'orderby' => 'date', 'category_name' => 'aktualnosci-prawa' );
         $postslist = get_posts( $args );
         foreach ($postslist as $post) :  setup_postdata($post); ?> 
           <div class="media">
             <a class="pull-left" href="<?php the_permalink(); ?>">
               <?php 
                 if ( has_post_thumbnail() ) {
                   the_post_thumbnail('thumbnail');
                 } 
               ?>
             </a>
             <div class="media-body">
               <a href="<?php the_permalink(); ?>"><strong><?php the_title(); ?> </strong></a>
               <small><?php the_time('j F, Y'); ?></small>
               <?php the_excerpt(); ?>
               <a href="<?php the_permalink(); ?>" class="btn btn-info">Czytaj więcej!</a>
             </div>
           </div>
           <hr>
           <?php endforeach; ?>
          </div>
        </div>
      </div> <!--col-md-6-->
    </div>
	
</div>
<?php get_footer(); ?>