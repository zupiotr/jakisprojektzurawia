<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

add_action( 'init', 'register_primary_menu' );
function register_primary_menu() {
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wptuts' ),
		'secondary' => __( 'Secondary Menu', 'wptuts' )
		) 	
	);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * navigation bootstrap
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
/**
	* logo header
 */






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function load_styles_and_scripts()  //funkcja w kt�rej b�d� umieszczone "hooki" umo�liwiaj�ce import plik�w CSS i JS do naszego motywu

{

	//wczytywanie CSS///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	wp_enqueue_style //wp_enqueue_style( $handle, $src ); gdzie $handle to przyjazna dla nas nazwa identyfikatora (np. style-bootstrap) a $src to �cie�ka
	(
    
	'bootstrap-styles', //nazwa identyfikatora - bootstrap styl
	get_template_directory_uri() . '/css/bootstrap.min.css'
	
	);

	wp_enqueue_style (
		'main-styles',  //g��wny styl
		get_template_directory_uri() . '/style.css'
	  );

	wp_enqueue_style (
		'font-awesome-style', //fontowy styl
		'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'
	  );
	

  // wczytywanie JS/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	  wp_enqueue_script (
		'jquery',  //jquery js
		get_template_directory_uri() . '/js/jquery.js',
		false,
		'3.1.1',
		true
	  );
	  
	  wp_enqueue_script (
		'script',  //script
		get_template_directory_uri() . '/js/script.js',
		false,
		'1.0',
		true
	  );

	  wp_enqueue_script (
		'bootstrap-scripts',  //bootstrap js
		get_template_directory_uri() . '/js/bootstrap.min.js',
		false,
		'3.3.7',
		true
	  );

}


/*funkcj� load_styles_and_scripts() i dopisujemy kolejn� linijk�, kt�ra uruchomi nasz� funkcj�. Ma ona posta� add_action('wp_enqueue_scripts', 'load_styles_and_scripts'); gdzie �load_styles_and_scripts� jak ju� pewnie zauwa�yli�cie to nazwa naszej funkcji importuj�cej style i skrypty.*/
add_action('wp_enqueue_scripts', 'load_styles_and_scripts');



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Tworzenie sidebara
 */
register_sidebar ( array(
  'name'          => 'main-sidebar',
  'description'   => 'Pasek boczny',
  //'before_widget' => '<aside id="%1$s" class="widget %2$s">',
  //'after_widget'  => '</aside>',
  //'before_title'  => '<h2 class="widget-title">',
  //'after_title'   => '</h2>',
  
  'before_widget' => '<section id="%1$s" class="widget %2$s">',
  'after_widget'  => '</section>',
  'before_title'  => '<h2 class="widget-title">',
  'after_title'   => '</h2>',
));


// Dodanie obs�ugi ikon wpis�w
add_theme_support( 'post-thumbnails' );


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




function themename_custom_logo_setup() {
    
	add_theme_support( 'custom-logo', array(
	
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );
	
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );



?>