<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title>
		<?php     // drukuje poprawny tytul strony w podgladzie kodu zrodlowego html                   
					if ( is_home () ) {//jesli to strona domowa                         
					bloginfo( 'name' ); //wyswietlenie nazy bloga w nagłówku                	 
					} 
					elseif ( is_archive() ) {//jeli to archiuwm                       
					single_cat_title(); 
					echo ' | ' ; bloginfo( 'name' );
					} 
					elseif ( is_singular() ) { //jesli pojedyncza strona postu                        
					single_post_title(); //wyswitlenie nazwy posta                  
					} 
					else {	wp_title( ' | ', true, 'right' ); //?                   
					}           	
		?>
	</title>
	<?php wp_head(); ?>
</head>
<body>
	
<div id="wrapper">
	<div id="header">
	
		<div id="nav">
				<nav class="navbar navbar-default" role="navigation">
					
				<!-- Brand and toggle get grouped for better mobile display --> 
				  <div class="navbar-header"> 
				  
				  <?php if( has_custom_logo() ) {
						the_custom_logo();
					} else { ?>
						<a class="navbar-brand" href="<?php bloginfo('url')?>"><?php bloginfo('name')?></a>
					<?php } ?>
					<button type="button" id="nav-icon4" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> 
					  <span class="sr-only"></span> 
					  <span class="icon-bar"></span> 
					  <span class="icon-bar"></span> 
					  <span class="icon-bar"></span> 
					</button>
					
					</div>
					
					<!-- Collect the nav links, forms, and other content for toggling --> 
					<div class="collapse navbar-collapse navbar-ex1-collapse"> 
						
						<?php
						   wp_nav_menu( array(
							   'menu'              => 'primary',
							   'theme_location'    => 'primary',
							   'depth'             => 2,
							   'container'         => 'false',
							   'container_class'   => 'collapse navbar-collapse',
							   'container_id'      => 'bs-example-navbar-collapse-1',
							   'menu_class'        => 'nav navbar-nav',
							   'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							   'walker'            => new wp_bootstrap_navwalker())
						   );
					   ?>
					</div>
				</nav>
		</div>
		
	</div>