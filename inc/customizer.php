<?php

function themeslug_theme_customizer( $wp_customize ) {
    // Fun code will go here
 $wp_customize->add_section( 'header_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
) );

 $wp_customize->add_setting( 'themeslug_logo' );
 $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'header_section',
    'settings' => 'themeslug_logo',
) ) );

$wp_customize->add_setting('phone_no');
    $wp_customize->add_control(
            'phone_no', array(
        'label' => 'Phone Number',
        'section' => 'header_section',
        'type' => 'text',
            )
    );
  
$wp_customize->add_setting('email');
    $wp_customize->add_control(
            'email', array(
        'label' => 'email',
        'section' => 'header_section',
        'type' => 'text',
            )
    );
 
$wp_customize->add_setting('facebook');
    $wp_customize->add_control(
            'facebook', array(
        'label' => 'facebook',
        'section' => 'header_section',
        'type' => 'text',
            )
    );
 
 $wp_customize->add_setting('twitter');
    $wp_customize->add_control(
            'twitter', array(
        'label' => 'twitter',
        'section' => 'header_section',
        'type' => 'text',
            )
    );
 
 $wp_customize->add_setting('copyrights');
    $wp_customize->add_control(
            'copyrights', array(
        'label' => 'copyrights',
        'section' => 'header_section',
        'type' => 'text',
            )
    );
 
 
}
add_action( 'customize_register', 'themeslug_theme_customizer' );